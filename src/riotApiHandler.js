class RiotApiHandler {

    constructor(apiKey) {
        this.apiKey = apiKey;
    }


    getChampionList() {
        return fetch(this.createChampionListRequest())
        .then(function(data) {
            if(data.ok) {
                console.log("CHAMPIONLIST DATA IS OK");
                return data.json();
            } else {
                console.log("CHAMPIONLIST DATA IS FAULTY");
                throw new Error(data.status);
            }
        })
    }


    createChampionListRequest(regionServer = "euw1", locale = "en_US", dataById = "false") {
        let endPoint = "static-data/v3/champions";
        let fullUrl = "/lol/" + endPoint + "?locale=" + locale + "&dataById=" + dataById + "&api_key=" + this.apiKey;
        return fullUrl;
    }
}


export default RiotApiHandler;
