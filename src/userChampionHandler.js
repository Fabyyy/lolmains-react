const CHAMPIONUSERDATAKEY = 'com.lolmains.championuserdata';


class UserChampionHandler {

    constructor() {
        this.championUserData = {};

        this.loadUserData(); // Initial load
    }

    getChampionUserData() {
        return this.championUserData;
    }

    loadUserData() {
        let browserData = localStorage.getItem(CHAMPIONUSERDATAKEY);
        var realUserData = {};
        if (browserData) {
            realUserData = JSON.parse(browserData);
        }
        this.championUserData = realUserData;
        return realUserData;
    }

    saveUserData() {
        localStorage.setItem(CHAMPIONUSERDATAKEY, JSON.stringify(this.championUserData));
    }

    setChampionData(championKey, positionShort, isActive) {
        let championUserData = this.championUserData[championKey];
        if (championUserData) {
            championUserData[positionShort] = isActive;
        } else {
            championUserData = {
                'top': false,
                'jng': false,
                'mid': false,
                'adc': false,
                'sup': false
            };
            championUserData[positionShort] = isActive;
        }
        this.championUserData[championKey] = championUserData;
        this.saveUserData();
    }

    getChampionData(championKey) {
        let championData = this.championUserData[championKey];
        if (championData) {
            return championData;
        } else {
            return {
                'top': false,
                'jng': false,
                'mid': false,
                'adc': false,
                'sup': false
            };
        }
    }
}

export default UserChampionHandler;
