import React, { Component } from 'react';

import RiotApiHandler from './riotApiHandler';
import UserChampionHandler from './userChampionHandler';

import './App.css';

import logo from './img/logo.svg';
import topicon from './img/topicon.png';
import jngicon from './img/jungleicon.png';
import midicon from './img/midicon.png';
import adcicon from './img/boticon.png';
import supicon from './img/supporticon.png';


class App extends Component {
    render() {
        return (
            <div className="App">
                <header className="App-header">
                    <img src={logo} className="App-logo" alt="logo" />
                    <h1 className="App-title">League Mains</h1>
                </header>
                <div>
                    <ChampionList />
                </div>
            </div>
        );
    }
}


class RoleFilterButton extends Component {
    render() {
        let filterButtonText = this.props.buttonText;
        let filterRoleIcon = this.props.buttonIcon;
        let roleShort = this.props.roleShort;
        let classList = this.props.isActive ? "role-filter-button active" : "role-filter-button";
        return (
            <a className={classList} onClick={() => {this.props.handleRoleFilteringClick(roleShort)}}>
                <img src={filterRoleIcon} alt={filterButtonText} />
                <div>{filterButtonText}</div>
            </a>
        );
    }
}


class RoleSelectionButton extends Component {
    handleRoleSelectionClick = () => {
        let roleShort = this.props.roleShort;
        let isActive = this.props.isActive;
        this.props.handleRoleSelection(roleShort, !isActive);
    }

    render() {
        let classList = this.props.isActive ? "role-selection-button active" : "role-selection-button";
        let roleIcon = this.props.roleIcon;
        let roleShort = this.props.roleShort;
        return (
            <a className={classList} onClick={this.handleRoleSelectionClick}>
                <img src={roleIcon} alt={roleShort} />
            </a>
        );
    }
}


class ChampionPane extends Component {
    handleRoleSelection = (roleShort, isActive) => {
        let championKey = this.props.championKey;
        this.props.handleChampionRoleSelection(championKey, roleShort, isActive);
    }

    render() {
        let championImgSource = "http://ddragon.leagueoflegends.com/cdn/" +
            this.props.championVersion + "/img/champion/" + this.props.championKey + ".png";
        let userChampionData = this.props.userChampionData;
        return (
            <div className="champion-wrapper">
                <img className="champion-image" src={championImgSource} alt={this.props.championKey} />
                <div className="champion-interaction-wrapper">
                    <RoleSelectionButton roleShort={"top"} roleIcon={topicon} isActive={userChampionData["top"]}
                        handleRoleSelection={this.handleRoleSelection} />
                    <RoleSelectionButton roleShort={"jng"} roleIcon={jngicon} isActive={userChampionData["jng"]}
                        handleRoleSelection={this.handleRoleSelection} />
                    <RoleSelectionButton roleShort={"mid"} roleIcon={midicon} isActive={userChampionData["mid"]}
                        handleRoleSelection={this.handleRoleSelection} />
                    <RoleSelectionButton roleShort={"adc"} roleIcon={adcicon} isActive={userChampionData["adc"]}
                        handleRoleSelection={this.handleRoleSelection} />
                    <RoleSelectionButton roleShort={"sup"} roleIcon={supicon} isActive={userChampionData["sup"]}
                        handleRoleSelection={this.handleRoleSelection} />
                </div>
            </div>
        );
    }
}


class ChampionList extends Component {
    constructor(props) {
        super(props);
        this.riotApiHandler = new RiotApiHandler(process.env.REACT_APP_RIOT_API_KEY);
        this.userChampionHandler = new UserChampionHandler();
        this.state = {
            championVersion: "",
            championData: {},
            userChampionData: {},
            currentChampionFilter: null
        }
    }

    componentDidMount() {
        // Initially load static champion data
        this.riotApiHandler.getChampionList()
        .then((data) => {
            console.log("Mounting Data", data.data);
            this.setState({
                championVersion: data.version,
                championData: data.data
            });
        })
        .catch((error) => {
            console.log("Mounting Error", error);
        });
        // Initially load user data
        let userChampionData = this.userChampionHandler.loadUserData();
        this.setState({
            userChampionData: userChampionData
        });
    }

    selectChampionFilter = (filterPositionShort) => {
        console.log("selectChampionFilter", filterPositionShort);
        let currentlySetPositionFilter = this.state.currentChampionFilter;
        let computedPositionFilter = filterPositionShort;
        if (currentlySetPositionFilter === filterPositionShort) {
            computedPositionFilter = null;
        }
        this.setState({
            currentChampionFilter: computedPositionFilter
        });
    }

    handleChampionRoleSelection = (championKey, positionShort, isActive) => {
        console.log("handleChampionRoleSelection", championKey, positionShort, isActive);
        this.userChampionHandler.setChampionData(championKey, positionShort, isActive);
        let userChampionData = this.userChampionHandler.getChampionUserData();
        this.setState({
            userChampionData: userChampionData
        });
    }

    render() {
        let championVersion = this.state.championVersion;
        let championData = this.state.championData;
        let sortedChampionKeys = Object.keys(championData).sort();
        let currentChampionFilter = this.state.currentChampionFilter;
        console.log("renderChampionlist", championVersion, championData, currentChampionFilter);
        return (
            <div className="champion-mains-app">
                <div className="champion-filter-container">
                    <RoleFilterButton
                        buttonText={"Top"}
                        buttonIcon={topicon}
                        roleShort={"top"}
                        isActive={currentChampionFilter === "top"}
                        handleRoleFilteringClick={this.selectChampionFilter}
                    />
                    <RoleFilterButton
                        buttonText={"Jungle"}
                        buttonIcon={jngicon}
                        roleShort={"jng"}
                        isActive={currentChampionFilter === "jng"}
                        handleRoleFilteringClick={this.selectChampionFilter}
                    />
                    <RoleFilterButton
                        buttonText={"Mid"}
                        buttonIcon={midicon}
                        roleShort={"mid"}
                        isActive={currentChampionFilter === "mid"}
                        handleRoleFilteringClick={this.selectChampionFilter}
                    />
                    <RoleFilterButton
                        buttonText={"ADC"}
                        buttonIcon={adcicon}
                        roleShort={"adc"}
                        isActive={currentChampionFilter === "adc"}
                        handleRoleFilteringClick={this.selectChampionFilter}
                    />
                    <RoleFilterButton
                        buttonText={"Support"}
                        buttonIcon={supicon}
                        roleShort={"sup"}
                        isActive={currentChampionFilter === "sup"}
                        handleRoleFilteringClick={this.selectChampionFilter}
                    />
                </div>
                <div className="champion-list-container">
                    {sortedChampionKeys.map((item, index) => {
                        let currentChampionData = this.userChampionHandler.getChampionData(item);
                        if (!currentChampionFilter || currentChampionData[currentChampionFilter]) {
                            return (
                                <ChampionPane key={index}
                                    championVersion={championVersion}
                                    championKey={item}
                                    championData={championData[item]}
                                    userChampionData={currentChampionData}
                                    handleChampionRoleSelection={this.handleChampionRoleSelection}
                                />
                            );
                        }
                    })}
                </div>
                <div className="filter-bar-blocker"></div>
            </div>
        )
    }
}


export default App;
